using Prova;
using Bogus;
using Bogus.DataSets;
using Bogus.Extensions.Brazil;

namespace Prova.UnitTests;

[CollectionDefinition("ExchangeRateCollection")]
public class ExchangeRateCollection : ICollectionFixture<ExchangeRateTestsFixture>
{}

public class ExchangeRateTestsFixture : IDisposable
{
    public ExchangeRate GenerateValidExchangeRate()
    {
        var exchangeRate = new ExchangeRate(
            DateOnly.FromDateTime(DateTime.UtcNow.AddDays(-10)),
            0.8930128m,
            Guid.NewGuid(),
            Guid.NewGuid(),
            Guid.NewGuid()
        );

        return exchangeRate;
    }

    public ExchangeRate GenerateValidExchangeRate(decimal factor)
    {
        var exchangeRate = new ExchangeRate(
            DateOnly.FromDateTime(DateTime.UtcNow.AddDays(-10)),
            factor,
            Guid.NewGuid(),
            Guid.NewGuid(),
            Guid.NewGuid()
        );

        return exchangeRate;
    }

    public ExchangeRate GenerateInvalidExchangeRate_InvalidFactor()
    {
        // var exchangeRate = new ExchangeRate(
        //     DateOnly.FromDateTime(DateTime.UtcNow.AddDays(-10)),
        //     -0.67m,
        //     Guid.NewGuid(),
        //     Guid.NewGuid(),
        //     Guid.NewGuid()
        // );

        var fakerExchangeRate = new Faker<ExchangeRate>("pt_BR")
            .CustomInstantiator(f => new ExchangeRate(
                DateOnly.FromDateTime(f.Date.Past(1, DateTime.UtcNow)),
                f.Random.Decimal(-5m, 0m),
                f.Random.Guid(),
                f.Random.Guid(),
                f.Random.Guid()
            ));

        return fakerExchangeRate;
    }

    public ExchangeRate GenerateInvalidExchangeRate_InvalidTypeId()
    {
        var exchangeRate = new ExchangeRate(
            DateOnly.FromDateTime(DateTime.UtcNow.AddDays(-10)),
            0.8930128m,
            new Guid(),
            Guid.NewGuid(),
            Guid.NewGuid()
        );

        return exchangeRate;
    }

    public ExchangeRate GenerateInvalidExchangeRate_InvalidQuotationDate()
    {
        var exchangeRate = new ExchangeRate(
            DateOnly.FromDateTime(DateTime.UtcNow.AddDays(10)),
            0.8930128m,
            Guid.NewGuid(),
            Guid.NewGuid(),
            Guid.NewGuid()
        );

        return exchangeRate;
    }

    public void Dispose()
    {
        // nenhum dado persistente para ser descartado
    }
}
