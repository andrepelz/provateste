namespace Prova.UnitTests;

[Collection("ExchangeRateCollection")]
public class ExchangeRateTests
{
    private readonly ExchangeRateTestsFixture _fixture;

    public ExchangeRateTests(ExchangeRateTestsFixture fixture)
    {
        _fixture = fixture;
    }

    [Fact(DisplayName = "New Valid ExchangeRate Correct Factor Precision")]
    [Trait("Category", "ExchangeRate Fixture Tests")]
    public void ExchangeRate_NewValidExchangeRate_FactorMustHavePrecisionOf5Digits()
    {
        int precision = 5;

        decimal factor = 0.123456m; // rounded up to 0.12346
        var exchangeRate = _fixture.GenerateValidExchangeRate(factor);

        var rounded_factor = exchangeRate.Factor;

        Assert.Equal(factor, rounded_factor, precision);
    }

    [Fact(DisplayName = "New Invalid ExchangeRate Invalid Factor")]
    [Trait("Category", "ExchangeRate Fixture Tests")]
    public void ExchangeRate_NewInvalidExchangeRate_NegativeFactorParameter_MustThrowException()
    {
        var domainException = Record.Exception(() => _fixture.GenerateInvalidExchangeRate_InvalidFactor());

        Assert.IsType<DomainException>(domainException);
        Assert.Equivalent("The Factor cannot be zero", domainException.Message, strict: true);
    }

    [Fact(DisplayName = "New Invalid ExchangeRate Invalid TypeId")]
    [Trait("Category", "ExchangeRate Fixture Tests")]
    public void ExchangeRate_NewInvalidExchangeRate_EmptyTypeIdParameter_MustThrowException()
    {
        var domainException = Record.Exception(() => _fixture.GenerateInvalidExchangeRate_InvalidTypeId());

        Assert.IsType<DomainException>(domainException);
        Assert.Equivalent("The TypeId cannot be empty", domainException.Message, strict: true);
    }

    [Fact(DisplayName = "New Invalid ExchangeRate Invalid QuotationDate")]
    [Trait("Category", "ExchangeRate Fixture Tests")]
    public void ExchangeRate_NewInvalidExchangeRate_FutureQuotationDateParameter_MustThrowException()
    {
        var domainException = Record.Exception(() => _fixture.GenerateInvalidExchangeRate_InvalidQuotationDate());

        Assert.IsType<DomainException>(domainException);
        Assert.Equivalent("The QuotationDate cannot be in the future", domainException.Message, strict: true);
    }
}
